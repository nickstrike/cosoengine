#include "Game.h"

Game::Game()
{
	i = 0;
}

Game::~Game()
{
}

bool Game::OnStart() 
{	
	i = 0;
	
	CollisionManager * instance = CollisionManager::GetInstance();
	cam = new  Camera(render);
	material1 = new Material();
	unsigned int programID1 = material1->LoadShaders("VertexTexture.txt", "FragmentTexture.txt");

	material2 = new Material();
	unsigned int programID2 = material2->LoadShaders("VertexTexture.txt", "FragmentTexture.txt");

	material3 = new Material();	
	unsigned int programID3 = material3->LoadShaders("colorvertexshader.txt", "colorfragmentshader.txt");	

	sprite1 = new Sprite(render, 3, 4);
	sprite1->SetMaterial(material1);
	sprite1->LoadMaterial("spritesheet_chicken.bmp");
	sprite1->SetPos(0, 0, 0);
	sprite1->SetBoundingBox(2.0f, 2.0f, false, 100);
	instance->SingUpToList(Layers::enemy, sprite1);
	sprite1->SetAnim(0, 8, 0.1f);

	sprite2 = new Sprite(render, 4, 4);
	sprite2->SetMaterial(material2);
	sprite2->LoadMaterial("spritesheet_caveman.bmp");
	sprite2->SetPos(2, 0, 0);
	sprite2->SetBoundingBox(2.0f, 2.0f, false, 20);
	instance->SingUpToList(Layers::player, sprite2);
	sprite2->SetAnim(0, 7, 0.1f);	
	
	sprite3 = new Sprite(render, 1, 1);
	sprite3->SetMaterial(material2);
	sprite3->LoadMaterial("angry.bmp");
	sprite3->SetPos(1, -10, 0);
	sprite3->SetBoundingBox(2.0f, 2.0f, false, 10);
	instance->SingUpToList(Layers::enemy, sprite3);

	triangle = new Triangle(render);
	triangle->SetMaterial(material3);

	rectangle = new Rectangle(render);
	rectangle->SetMaterial(material3);
	circle = new Circle(render, 1, 20);
	circle->SetMaterial(material3);

	triangle->SetPos(-3, 5, 0);
	rectangle->SetPos(3, 5, 0);
	circle->SetPos(0, 5, 0);

	cout << "Game::OnStart()" << endl;
	return true;
}

bool Game::OnStop() {

	delete material1;
	delete material2;
	delete material3;

	delete triangle;
	delete rectangle;
	delete circle;
	
	delete sprite1;
	delete sprite2;
	delete sprite3;

	cout << "Game::OnStop()" << endl;
	return false;
}

bool Game::OnUpdate() {
	i++;
	cout << "Game::OnUpdate(): " << i << endl;	
	CollisionManager::GetInstance()->UpdatePhysicsBox();

	cam->Roll(0.01);
	cam->Walk(0.00);
	cam->Strafe(0.00);

	sprite2->UpdAnim(deltaTime);
	sprite1->UpdAnim(deltaTime);
	triangle->SetRot(0.0f, 0.0f, i / 5);
	rectangle->SetRot(0.0f, 0.0f, i / 5);
	circle->SetRot(0.0f, 0.0f, i / 5);
	
	sprite2->Translate(-2 * deltaTime, 0, 0);
	sprite3->Translate(0, 2* deltaTime, 0);

	return true;
}

void Game::OnDraw()
{
	sprite3->Draw();
	sprite1->Draw();
	sprite2->Draw();	
	triangle->Draw();
	rectangle->Draw();
	circle->Draw();

	cout << "Game::OnDraw(): " << i << endl;
}
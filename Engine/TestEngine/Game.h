#include "Gamebase.h"
#include "Triangle.h"
#include "Circle.h"
#include "Rectangle.h"
#include "Material.h"
#include "TextureImporter.h"
#include "Sprite.h"
#include "Camera.h"
#include "CollisionManager.h"
#include <iostream>

class Game : public GameBase {
private:
	int i;
	Sprite * sprite1;
	Sprite * sprite2;
	Sprite * sprite3;

	Triangle* triangle;
	Rectangle* rectangle;
	Circle* circle;

	Material * material1;
	Material * material2;
	Material * material3;
	Material * material4;
	Camera *cam;
public:
	Game();
	~Game();
protected:
	bool OnStart() override;
	bool OnStop() override;
	bool OnUpdate() override;
	void OnDraw() override;
};
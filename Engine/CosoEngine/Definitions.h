#pragma once
#ifndef DEFINITIONS_H
#define DEFINITIONS_H

enum  Layers
{
	player = 0,
	enemy,
	map,
	count
};
enum ProjectionMatrixType
{
	perspective = 0,
	orthographic

};
#endif 

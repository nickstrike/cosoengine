#pragma once
#include "Exports.h"
#include "Renderer.h"
#include "Entity.h"
#include "Definitions.h"
#include "Window.h"
#include <glm.hpp>
#include <gtc/matrix_transform.hpp>
#include <GLFW/glfw3.h>
#include <iostream>

class COSOENGINE_API Camera
{
private:
	Renderer* renderer;	
	glm::vec3 camPos;
	glm::vec3 eyePos;
	glm::vec3 upPos;

	glm::vec4 forward;
	glm::vec4 right;
	glm::vec4 upDir;

public:
	void Update();
	void Walk(float direction);
	void Strafe(float direction);
	void Pitch(float direction);
	void Yaw(float direction);
	void Roll(float direction);

	Camera(Renderer * render);
	~Camera();
};
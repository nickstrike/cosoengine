#include "Camera.h"


Camera::Camera(Renderer *render)
{
	renderer = render;
	eyePos = glm::vec3(0.0f, 0.0f, 10.0f);
	upPos = glm::vec3(0.0f, 1.0f, 0.0f);


	forward = glm::vec4(0.0f, 0.0f, -1.0f, 0.0f);
	right = glm::vec4(1.0f, 0.0f, 0.0f, 0.0f);
	upDir = glm::vec4(0.0f, 1.0f, 0.0f, 0.0f);

	camPos = eyePos + (glm::vec3)forward;
}

Camera::~Camera()
{
}

void Camera::Update()
{
	renderer->SetViewMatrix(eyePos, camPos, upPos);
}
void Camera::Walk(float dir)
{
	camPos += (glm::vec3)forward * dir;
	eyePos += (glm::vec3)forward * dir;
	Update();
}
void Camera::Strafe(float dir)
{
	camPos += (glm::vec3)right * dir;
	eyePos += (glm::vec3)right * dir;
	Update();
}
void Camera::Pitch(float dir)
{
	forward = glm::rotate(glm::mat4(1.0f), dir, glm::vec3(right.x, right.y, right.z)) * forward;
	upDir = glm::rotate(glm::mat4(1.0f), dir, glm::vec3(right.x, right.y, right.z)) * upDir;

	upPos = (glm::vec3)upDir;
	camPos = eyePos + (glm::vec3)forward;
	Update();
}
void Camera::Yaw(float dir)
{
	forward = glm::rotate(glm::mat4(1.0f), dir, glm::vec3(upPos.x, upPos.y, upPos.z)) * forward;
	right = glm::rotate(glm::mat4(1.0f), dir, glm::vec3(upPos.x, upPos.y, upPos.z)) * right;

	upPos = (glm::vec3)upDir;
	camPos = eyePos + (glm::vec3)forward;
	Update();
}
void Camera::Roll(float dir)
{
	glm::mat4 rot = glm::rotate(glm::mat4(1.0f), dir, glm::vec3(forward.x, forward.y, forward.z));
	right = rot * right;
	upDir = rot * upDir;

	upPos = (glm::vec3)upDir;
	camPos = eyePos + (glm::vec3)forward;
	Update();
}



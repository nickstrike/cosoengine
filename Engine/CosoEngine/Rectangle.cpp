#include "Rectangle.h"

Rectangle::Rectangle(Renderer *render) :Shape(render)
{
	vertex = new float[12]
	{
		-1.0f,-1.0f , 0.0f ,
		1.0f,-1.0f , 0.0f ,
		-1.0f, 1.0f , 0.0f ,
		1.0f, 1.0f , 0.0f
	};

	colorVertex = new float[12]
	{
		0.555f,  0.365f,  0.945f,
		0.945f,  0.774f,  0.589f,
		0.859f,  0.258f,  0.679f,
		0.110f,  0.411f,  0.912f
	};

	SetVertices(vertex, 4);
	SetColorVertex(colorVertex, 4);
}

Rectangle::~Rectangle()
{
	delete[] vertex;
	delete[] colorVertex;
}


void Rectangle::Draw()
{
	DrawMesh(GL_TRIANGLE_STRIP);
}


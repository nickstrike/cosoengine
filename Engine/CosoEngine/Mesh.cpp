#include "Mesh.h"

Mesh::Mesh(Renderer *render, const char* filename, const char* texturename, vector<BMPheader*> textures, vector<MeshData>*data) :Shape(render)
{	
	vector<BMPheader*> *textures = new vector<BMPheader*>();
	vector<MeshData> *data = new vector<MeshData>();
}

Mesh::~Mesh()
{
}

void Mesh::Draw()
{
	DrawMesh(GL_TRIANGLES);
}

void Mesh::DrawMesh(int drawType)
{
	render->LoadIMatrix();
	render->SetWMatrix(WorldMatrix);

	if (material != NULL) {
		material->BindProgram();
		material->Bind("WVP");
		material->SetMatrixProperty(render->GetWVP());
	}
	render->BeginDraw(0);	
	render->BeginDraw(1);
	render->BindBuffer(0, bufferId, 3);
	render->BindColorBuffer(colorBufferId, 1);
	render->BindMeshBuffer(indexBufferId);
	render->DrawIndex(idxVtxCount);
	render->EndDraw(0);
	render->EndDraw(1);
}


#pragma once
#include "Entity.h"
#include "BoundingBox.h"
#include "Material.h"


class COSOENGINE_API Shape : public Entity
{
protected:
	Material * material;

	bool shouldDispose;
	bool shouldDisposeColor;

	int vertexCount;
	int colorVertexCount;
	int idxVtxCount;

	unsigned int colorBufferId;
	unsigned int bufferId;
	unsigned int indexBufferId;
	unsigned int * indxvertex;
	float * vertex;
	float * colorVertex;
	

public:
	Shape(Renderer * render);
	virtual void Draw() = 0;
	virtual void DrawMesh(int typeDraw);
	void SetVertices(float * vertices, int count);
	void SetColorVertex(float * vertices, int count);
	void SetIndexVertices(unsigned int* vertices, int count);
	void SetMaterial(Material* material);
	void Dispose();
	void DisposeColor();
	~Shape();
};


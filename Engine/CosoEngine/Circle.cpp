#include "Circle.h"

Circle::Circle(Renderer *render, float rdio, int cantTri) :Shape(render)
{
	cantTriangles = cantTri;
	radius = rdio;
	vertexCount = cantTriangles * 3;
	degrees = 360.0f / cantTriangles;
	angle = 0.0f;

	vertex = new float[vertexCount] {};
	colorVertex = new float[vertexCount] {};
	vec3 vec;

	for (int i = 0; i < vertexCount; i += 3)
	{
		vec = vec3(cos(angle), sin(angle), 0) * radius;
		vertex[i] = vec.x;
		vertex[i + 1] = vec.y;
		vertex[i + 2] = vec.z;
		angle += degrees * PI / 180.0f;

	}
	SetVertices(vertex, cantTriangles);

	for (int i = 0; i < vertexCount; i += 3)
	{
		vec = vec3(cos(angle), sin(angle), 0) * radius;
		colorVertex[i] = vec.x;
		colorVertex[i + 5] = vec.y;
		colorVertex[i + 11] = vec.z;
		angle += degrees * PI / 180.0f;

	}

	SetColorVertex(colorVertex, cantTriangles);
}


Circle::~Circle()
{
	delete[] vertex;
}

void Circle::Draw()
{
	DrawMesh(GL_TRIANGLE_FAN);
}

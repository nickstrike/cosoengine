#include "BoundingBox.h"

BoundingBox::BoundingBox(glm::vec2 pos, unsigned int w, unsigned int h, bool setStatic, float m) :
	boxPosition(pos), width(w), height(h), isStatic(setStatic), collision(false), mass(m) {
}

float BoundingBox::GetX()
{
	return boxPosition.x;
}

float BoundingBox::GetY()
{
	return boxPosition.y;
}

glm::vec2 BoundingBox::GetPos()
{
	return boxPosition;
}

void BoundingBox::SetPos(float x, float y) {
	boxPosition.x = x;
	boxPosition.y = y;
}

float BoundingBox::GetWidth()
{
	return width;
}

float BoundingBox::GetHeigth()
{
	return height;
}

bool BoundingBox::GetCollision()
{
	return collision;
}

float BoundingBox::GetMass()
{
	return mass;
}

void BoundingBox::SetCollision(bool value)
{
	collision = value;
}

BoundingBox::~BoundingBox() {
}

bool BoundingBox::GetStatic()
{
	return isStatic;
}

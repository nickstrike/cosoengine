#pragma once
#include "Exports.h"
#include "Definitions.h"
#include "Window.h"
#include <glm.hpp>
#include <gtc/matrix_transform.hpp>
#include <vector>
#include <GLFW/glfw3.h>
#include <iostream>
using namespace std;
using namespace glm;
class COSOENGINE_API Renderer
{
private:
	Window* win;
	unsigned int VertexArrayID;
	unsigned int ColorVertexArrayID;
	glm::mat4 WorldMatrix;
	glm::mat4 ViewMatrix;
	glm::mat4 ProjectionMatrix;
	glm::mat4 orthoProjectionMatrix;
	glm::mat4 perspectiveProjectionMatrix;
	glm::mat4 WVP;

	glm::vec3 camPos;
	glm::vec3 eyePos;
	glm::vec3 upPos;

public:
	bool Start(Window* wnd);
	bool Stop();
	void setClearScreenColor(float r, float g, float b, float a);
	unsigned int GenBuffer(float* buffer, int size);
	unsigned int GenIndexBuffer(unsigned int* buffer, int size);
	unsigned int GenIndexBuffer(vector<unsigned int> index);
	void BeginDraw(unsigned int atribID);
	void EndDraw(unsigned int atribID);
	void BindBuffer(unsigned int atribID, unsigned int vtxBuffer, unsigned int size);
	void BindColorBuffer(unsigned int clrbuffer, unsigned int name);
	void BindMeshBuffer(unsigned int idxbuffer);
	unsigned int ChargeTexture(unsigned int width, unsigned int height, unsigned char * data);
	void BindTexture(unsigned int texture, unsigned int textureID);
	void DrawBuffer(int size, int typeDraw);
	void DrawIndex(int idxcount);
	void DestroyBuffer(unsigned int buffer);
	void ClearScreen();
	void SwapBuffer();
	void UpdateWVP();
	glm::mat4& GetWVP();
	void TranslateCamera(glm::vec3 pos);
	void LoadIMatrix();
	void SetWMatrix(glm::mat4 matrix);
	void MultiplyWMatrix(glm::mat4 matrix);
	void SetOrthoProjectionMatrix(const float left, const float right, const float bottom, const float top, const float ZNear, const float ZFar);
	void SetPerspectiveProjectionMatrix(const float angle, const float aspect, const float ZNear, const float ZFar);
	void SwitchProjectionMatrix(ProjectionMatrixType pmt);
	void SetViewMatrix(glm::vec3 eye, glm::vec3 cam, glm::vec3 up);
	glm::vec3 GetCameraPos();

	Renderer();
	~Renderer();
};
#pragma once
#include "Shape.h"
#include "Material.h"
class COSOENGINE_API Triangle :public Shape
{

public:
	Triangle(Renderer *render);
	~Triangle();	
	void Draw() override;
};
#pragma once
#include "Shape.h"
#define PI 3.1415f

class COSOENGINE_API Circle: public Shape
{
private:
	float radius;
	float angle;
	float degrees;
	int cantTriangles;
public:
	Circle(Renderer *render, float rdio, int trCant);
	~Circle();
	void Draw() override;
};


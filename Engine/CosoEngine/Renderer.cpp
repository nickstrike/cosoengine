#include "Renderer.h"
#include <GL\glew.h>
#include <GLFW\glfw3.h>
using namespace std;

bool Renderer::Start(Window* wnd) {
	cout << "Renderer::Start()" << endl;
	win = wnd;
	glfwMakeContextCurrent((GLFWwindow*)wnd->GetWindowPtr());

	if (glewInit() != GLEW_OK) {
		cout << "Fall� al inicializar GLEW\n" << endl;
		return -1;
	}

	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);
	perspectiveProjectionMatrix = glm::perspective(90.0f, 1.5f, 0.0f, 100.f);
	orthoProjectionMatrix = glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, 0.0f, 100.f);
	ProjectionMatrix = perspectiveProjectionMatrix;		
														
	camPos = glm::vec3(0, 0, 5); //DONDE ESTA			
	eyePos = glm::vec3(0, 0, 0); //DONDE MIRA			
														
	ViewMatrix = glm::lookAt(							
		camPos, 
		eyePos,
		glm::vec3(0, 1, 0)
	);

	WorldMatrix = glm::mat4(1.0f);

	UpdateWVP();

	return true;
}

bool Renderer::Stop() {
	cout << "Renderer::Stop()" << endl;
	return true;
}

void Renderer::setClearScreenColor(float r, float g, float b, float a) {
	glClearColor(r, g, b, a);
}

unsigned int Renderer::GenBuffer(float * buffer, int size)
{
	unsigned int vertexBuffer;
	glGenBuffers(1, &vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, size, buffer, GL_STATIC_DRAW);
	return vertexBuffer;
}

unsigned int Renderer::GenIndexBuffer(unsigned int* buffer, int size)
{
	unsigned int idxbuffer;
	glGenBuffers(1, &idxbuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, idxbuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, size * sizeof(unsigned int), &buffer[0], GL_STATIC_DRAW);

	return idxbuffer;
}

unsigned int Renderer::GenIndexBuffer(vector<unsigned int> index)
{
	unsigned int idxbuffer;
	glGenBuffers(1, &idxbuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, idxbuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, index.size() * sizeof(unsigned int), &index[0], GL_STATIC_DRAW);

	return idxbuffer;
}

void Renderer::BeginDraw(unsigned int atribID) {
	glEnableVertexAttribArray(atribID);
}

void Renderer::EndDraw(unsigned int atribID) {
	glDisableVertexAttribArray(atribID);
}

void Renderer::BindBuffer(unsigned int atribID, unsigned int vtxBuffer, unsigned int size) {
	glBindBuffer(GL_ARRAY_BUFFER, vtxBuffer);
	glVertexAttribPointer(
		atribID,            // debe corresponder en el shader.
		size,               // tama�o
		GL_FLOAT,           // tipo
		GL_FALSE,           // normalizado?
		0,                  // Paso
		(void*)0            // desfase del buffer
	);
}

void Renderer::BindColorBuffer(unsigned int clrbuffer, unsigned int atribId)
{
	glBindBuffer(GL_ARRAY_BUFFER, clrbuffer);
	glVertexAttribPointer(
		atribId,														// Le paso la ubicacion de donde se guardo la mempria del vertice
		3,																// tama�o
		GL_FLOAT,														// tipo
		GL_FALSE,														// normalizado?
		0,																// Paso
		(void*)0														// desfase del buffer
	);

}

void Renderer::BindMeshBuffer(unsigned int idxbuffer)
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, idxbuffer);

}

void Renderer::DrawIndex(int idxcount)
{
	glDrawElements(
		GL_TRIANGLES,
		idxcount,
		GL_UNSIGNED_INT,
		(void*)0
	);
}

unsigned int Renderer::ChargeTexture(unsigned int width, unsigned int height, unsigned char * data)
{
	unsigned int textureID;
	glGenTextures(1, &textureID);

	glBindTexture(GL_TEXTURE_2D, textureID);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, data);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	glGenerateMipmap(GL_TEXTURE_2D);

	return textureID;
}

void Renderer::BindTexture(unsigned int texture, unsigned int textureID)
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);
	glUniform1i(textureID, 0);
}

void Renderer::DrawBuffer(int size, int typeDraw)
{
	glDrawArrays(typeDraw, 0, size);
}

void Renderer::DestroyBuffer(unsigned int buffer)
{
	glDeleteBuffers(1, &buffer);
}

void Renderer::ClearScreen() {
	glClear(GL_COLOR_BUFFER_BIT);
}

void Renderer::SwapBuffer() {
	glfwSwapBuffers((GLFWwindow*)win->GetWindowPtr());
}

void Renderer::UpdateWVP()
{
	WVP = ProjectionMatrix * ViewMatrix * WorldMatrix;
}

glm::mat4 & Renderer::GetWVP()
{
	return WVP;
}

void Renderer::TranslateCamera(glm::vec3 pos)
{

	camPos += pos;
	eyePos += glm::vec3(pos.x, pos.y, 0);

	ViewMatrix = glm::lookAt(
		camPos,
		eyePos,
		glm::vec3(0, 1, 0)
	);

	UpdateWVP();
}

void Renderer::LoadIMatrix()
{
	WorldMatrix = glm::mat4(1.0f);
}

void Renderer::SetWMatrix(glm::mat4 matrix)
{
	WorldMatrix = matrix;
	UpdateWVP();
}

void Renderer::MultiplyWMatrix(glm::mat4 matrix)
{
	WorldMatrix *= matrix;
	UpdateWVP();
}

glm::vec3 Renderer::GetCameraPos()
{
	return camPos;
}

void Renderer::SetOrthoProjectionMatrix(const float left, const float right, const float bottom, const float top, const float ZNear, const float ZFar)
{
	orthoProjectionMatrix = glm::ortho(left, right, bottom, top, ZNear, ZFar);
}
void Renderer::SetPerspectiveProjectionMatrix(const float angle, const float aspect, const float ZNear, const float ZFar)
{
	perspectiveProjectionMatrix = glm::perspective(angle, aspect, ZNear, ZFar);
}
void Renderer::SetViewMatrix(glm::vec3 eye, glm::vec3 cam, glm::vec3 up)
{
	camPos = cam;
	eyePos = eye;
	upPos = up;

	ViewMatrix = glm::lookAt(
		camPos,
		eyePos,
		upPos
	);

	UpdateWVP();
}
void Renderer::SwitchProjectionMatrix(ProjectionMatrixType pmt)
{
	if (pmt == 0)
	{
		ProjectionMatrix = perspectiveProjectionMatrix;
	}
	else
	{
		if (pmt == 1)
		{
			ProjectionMatrix = orthoProjectionMatrix;
		}
	}
}

Renderer::Renderer() {
}


Renderer::~Renderer() {
}
#pragma once
#include "Exports.h"
#include "Definitions.h"
#include <iostream>
#include<glm.hpp>
#include<gtc\matrix_transform.hpp>
using namespace std;
using namespace glm;

class COSOENGINE_API BoundingBox
{
private:
	vec2 boxPosition;
	float height;
	float width;
	bool isStatic;
	float mass;
	bool collision;

public:
	BoundingBox(vec2 pos, unsigned int width, unsigned int heigth, bool setStatic, float mass);
	~BoundingBox();
	float GetX();
	float GetY();
	vec2 GetPos();
	void SetPos(float x, float y);
	float GetWidth();
	float GetHeigth();
	float GetMass();
	bool GetStatic();
	bool GetCollision();
	void SetCollision(bool value);
};
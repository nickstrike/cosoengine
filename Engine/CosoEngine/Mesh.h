#pragma once
#include "Exports.h"
#include "Shape.h"
#include<vector>
#include "TextureImporter.h"
using namespace std;
struct MeshData
{
	MeshData *meshinfo;
	vector<float> vertexVec;
	vector<float> uvVec;
	vector<float> normalVec;
	vector<unsigned int> indexVec;

};

class COSOENGINE_API Mesh :public Shape
{
private:


public:
	Mesh(Renderer *render, const char* filename, const char* texturename, vector<BMPheader*> textures, vector<MeshData>*data);
	~Mesh();
	void Draw();
	void DrawMesh(int typeDraw) override;
};

